-- [SECTION] CRUD Operations

--Inserting Records (Create)

--To insert an artist in the artists table
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Rivermaya");

--To insert albums in the albums table:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 1);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 2);

---To insert songs in the songs table:
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Ice Cream', '00:04:16', 'Kpop', 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('You Never Know', '00:03:59', 'kpop', 1);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Kundiman', '00:03:54', 'OPM', 2); 
INSERT INTO songs (song_name, length, genre, album_id) VALUES ('Kisapmata', '00:04:39', 'OPM', 2);

--Read

--Display the title and genre of all the songs.
SELECT song_name, genre FROM songs;
--Display all the field name;
SELECT * from songs;
--Display the title of all the OPM songs.
SELECT song_name FROM songs WHERE genre = "OPM";
--Display the title and length of the k-pop songs that are more than 400seconds
SELECT song_name, length FROM songs WHERE length > 400 and genre = "Kpop";



--Updating Records

--Update the lengh of You Never Know To 00:04:00
--UPDATE <table_name> SET <field_name> = <Value> WHERE <val_field> = <Value>;
UPDATE songs SET length = 400 WHERE song_name = "You Never Know";

--Deleting Records

--Delete all K-pop songs that are more than 4:00 minutes.
DELETE FROM songs WHERE genre = "Kpop" AND length > 400;

--Removing the WHERE clause will delete all rows.
DELETE FROM songs;